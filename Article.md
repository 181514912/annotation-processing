# Annotation Processing

Annotation is a tagging mechanism for classes or its member variable and methods. It was introduced in Java 1.5 and since then it has become one of the most powerful features available.


## Where Annotations are used?

To understand a simple use of annotation, let us consider an `Animal` class which has a `makeSoundWhenHappy()` method.
```java
class Animal {
	public void makeSoundWhenHappy() {
		// some codes
	}
}
```
Suppose a `Dog` class inherits from this `Animal` class as shown in the following code.
```java
class Dog extends Animal {
	public void makeSoundWhanHappy() {
		// some codes
	}
}
```
The `Dog` class overrides the `makeSoundWhenHappy()` method of `Animal` class, but suppose the developer had made a spelling mistake in the function name. Can you identify that? Its 'a' instead of 'e'. The code will compile perfectly without any warning, but the intended functionality will not be achieved.

Here comes the use of annotation `@Override`. If used over the method it will show an error during compilation that the two methods are not the same.

```java
class Dog extends Animal {
	@Override
	public void makeSoundWhanHappy() {	// will show error
		// some codes
	}
}
```
There are many other places where annotations are used. Some of them are:
* `@Deprecated` Annotation is used to mark the method as obsolete. It will cause a compiler warning if that method is used.
* `@Component` is used in the Spring framework to mark a class as a Bean.
* `@WebServlet` is used to map a servlet to a particular URL.

## Benefits of Annotation
Some of the uses of Annotation are :
* They provide information for the *compiler*.
* They also provide information to other developers (e.g. `@Override`).
* They are used as an alternative for XML configurations. Software tools can process annotation information to generate code and XML files.
* They are also helpful in deployment-time as well as during runtime processing.

## Creating a custom Annotation

We can create our own annotation using `@interface` keyword. Let us create a `Dog` class which has two properties `sound` and `legs`. But these properties will be provided by our `@Animal` annotation which can be created as follow.
```java
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface Animal {
	String sound() default "silent";
	int legs() default 0;
}
```
Let us first explain this code. We can see three annotations above our custom annotation. They are called **meta-annotation** and used to provide information such as:
* `@Documented` annotation is used to mark that our new annotation should be present in the generated document.
* `@Target` is used to specify the level at which this annotation should be used. We are using the class-level annotation.
	* `ElementType.TYPE` specify the class level annotation.
	* `ElementType.METHOD` specify the method level annotation.
	* `ElementType.FIELD` specify the field level annotation.
	* `ElementType.CONSTRUCTOR` specify the constructor level annotation.
* `@Retention` is used to tell the processing level till which to retain our annotation. Our annotation is present at runtime also.
	* `RetentionPolicy.SOURCE` specifies that the annotation is only present in the source code and should not be present in `.class` files.
	* `RetentionPolicy.CLASS` specify that the annotation should also be present in `.class` files, but not during runtime.
	* `RetentionPolicy.RUNTIME` specifies that the annotation should also be present during the runtime of the program.

Let us now use this annotation to mark our `Dog` class.
```java
@Animal(sound = "woof", legs = 4)
class Dog {
}
```
We can provide new values to our annotation fields as shown above. If they are omitted than default values will be used. Code for testing our custom annotation is as shown below. 
```java
public class Main {
	public static void main(String[] args) {
		// creating dog object
		Dog dog = new Dog();

		// using Reflection to fetch properties
		Class c = dog.getClass();
		Annotation an = c.getAnnotation(Animal.class);
		Animal s = (Animal) an;

		// printing values
		System.out.println(s.sound());
		System.out.println(s.legs());
	}
}
```

Note that we are using **Reflection API** to fetch the value of the annotation. It is another powerful feature that helps us to get properties of an object and modify/invoke them at runtime. The output of the above program is as shown below.
```java
woof
4
```

This is just a demonstration of how annotation can be used. Annotation processing can make things simpler which otherwise would be time-consuming and hard to maintain.

## References

1. [Oracle Docs](https://docs.oracle.com/javase/tutorial/java/annotations/basics.html)
2. [wikipedia.org/wiki/Java_annotation](https://en.wikipedia.org/wiki/Java_annotation)
3. [javacodegeeks.com/2015/09/java-annotation-processors](https://www.javacodegeeks.com/2015/09/java-annotation-processors.html)